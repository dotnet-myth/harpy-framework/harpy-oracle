﻿using Hangfire.Oracle.Core;
using System;

namespace Harpy.Oracle.Presentation.Extensions
{

    public static class HangfireOracle {

        public static OracleStorage UseStorage( string hangfireConnection ) {
            if ( string.IsNullOrEmpty( hangfireConnection ) )
                throw new ArgumentNullException( hangfireConnection, "Connection string is not valid!" );

            var options = new OracleStorageOptions {
                TransactionTimeout = TimeSpan.FromMinutes( 15 ),
                JobExpirationCheckInterval = TimeSpan.FromMinutes( 15 )
            };

            return new OracleStorage( hangfireConnection, options );
        }
    }
}
