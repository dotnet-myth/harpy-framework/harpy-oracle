﻿using Microsoft.EntityFrameworkCore;
using Oracle.EntityFrameworkCore.Infrastructure;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;

namespace Harpy.Oracle.IoC.Extensions
{

    public static class OracleContainer {

        public static IDbConnection UseOracleConnection( this DbContextOptionsBuilder dbContextOptions, string connectionString = null, Action<OracleDbContextOptionsBuilder> oracleOptionsAction = null ) {
            if ( string.IsNullOrEmpty( connectionString ) )
                throw new Exception( "Connection string is not valid" );

            var dbConnection = new OracleConnection( connectionString );
            dbConnection.Open( );

            dbContextOptions.UseOracle( dbConnection, oracleOptionsAction );

            return dbConnection;
        }

        public static OracleDbContextOptionsBuilder AddMigrations( this OracleDbContextOptionsBuilder optionsBuilder, string migrationsAssembly = "" ) =>
            optionsBuilder.MigrationsAssembly( migrationsAssembly ).MigrationsHistoryTable( "migrations_history" );

        public static OracleDbContextOptionsBuilder UseCompatibility( this OracleDbContextOptionsBuilder optionsBuilder, string version ) {
            optionsBuilder.UseOracleSQLCompatibility( version );
            return optionsBuilder;
        }
    }
}
